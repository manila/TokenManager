package dk.manila.token_manager.exception;

/**
 * Exception throws when a CprNumber is not correct
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class CprException extends Exception {

	private static final long serialVersionUID = -7748609687044491887L;

	/**
	 * Constructor of the class CprException
	 * 
	 * @param msg
	 *            message of the exception
	 */
	public CprException(String msg) {
		super(msg);
	}
}
