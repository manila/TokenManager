package dk.manila.token_manager.exception;

/**
 * Exception throws when a Token is not correct
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class TokenException extends Exception {

	private static final long serialVersionUID = 6623763784880245664L;

	/**
	 * Default constructor of TokenException
	 */
	public TokenException() {
	}

	/**
	 * Constructor of TokenException with a message
	 * 
	 * @param message
	 *            message of the exception
	 */
	public TokenException(String message) {
		super(message);
	}

}
