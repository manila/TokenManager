package dk.manila.token_manager.impl;

import java.io.IOException;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator;

/**Class for genrating the png image of a token as a barcode
 * 
 * @author sebastiannyholm
 *
 */
public class TokenBarcodeImageGeneratorImpl implements TokenBarcodeImageGenerator {

	
	@Override
	public byte[] generateBarcodeImage(String tokenId, int width, int heigh, BarcodeFormat barcodeFormat, String imageType) throws BarcodeImageGenerationException {
	
		BitMatrix barcode;
		try {
			barcode = new Code128Writer().encode(tokenId, barcodeFormat, width, heigh);
		} catch (WriterException e) {
			throw new BarcodeImageGenerationException(e.getMessage()); 
		}
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			MatrixToImageWriter.writeToStream(barcode, imageType, out);
		} catch (IOException e) {
			throw new BarcodeImageGenerationException(e.getMessage());
		}

		return (byte[]) out.toByteArray();
	}
	
}
