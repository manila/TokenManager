package dk.manila.token_manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;

/**
 * Implication of a TokenRepository
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class TokenRepositoryImpl implements TokenRepository {

	private Map<CprNumber, List<Token>> tokens;

	/**
	 * Constructor of the class TokenRepositoryImpl
	 */
	public TokenRepositoryImpl() {
		tokens = new HashMap<CprNumber, List<Token>>();
	}

	@Override
	public void addToken(CprNumber cpr, Token token) {
		tokens.get(cpr).add(token);
	}

	@Override
	public void addTokens(CprNumber cpr, List<Token> tokens) {
		for (Token t : tokens)
			addToken(cpr, t);
	}

	@Override
	public boolean containsCustomer(CprNumber cpr) {
		return tokens.containsKey(cpr);
	}

	@Override
	public void addCustomer(CprNumber cpr) {
		tokens.put(cpr, new ArrayList<Token>());
	}

	@Override
	public Token getToken(TokenId tokenId) {
		
		for (CprNumber cpr : tokens.keySet())
			for (Token t : tokens.get(cpr))
				if (t.getTokenId().equals(tokenId))
					return t;
		
		return null;
	}

	@Override
	public CprNumber getCprByTokenId(TokenId tokenId) {
		for (CprNumber cpr : tokens.keySet())
			for (Token t : tokens.get(cpr))
				if (t.getTokenId().equals(tokenId))
					return cpr;
		return null;
	}

	@Override
	public List<Token> getTokens(CprNumber cpr) {
		List<Token> myTokens = new ArrayList<Token>();

		if (tokens.containsKey(cpr)) {
			myTokens = tokens.get(cpr);
		}

		return myTokens;
	}
	
	@Override
	public void setTokenAsUsed(TokenId tokenId) {
		for (CprNumber cpr : tokens.keySet())
			for (Token t : tokens.get(cpr))
				if (t.getTokenId().equals(tokenId))
					t.setUsed(true);
	}

	@Override
	public boolean isTokenUsed(TokenId tokenId) {
		
		boolean used = false;
		for (CprNumber cpr : tokens.keySet())
			for (Token t : tokens.get(cpr))
				if (t.getTokenId().equals(tokenId) && t.isUsed()) {
					used = true;
				}
		
		return used;
	}

}