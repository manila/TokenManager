package dk.manila.token_manager.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.google.zxing.BarcodeFormat;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.queue.Event;
import dk.manila.token_manager.queue.EventReceiver;
import dk.manila.token_manager.queue.EventSender;

/**
 * Implementation of TokenService
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class TokenServiceImpl implements TokenService, EventReceiver  {

	private TokenRepository tokenRepository;
	private EventSender eventSender;
	private TokenBarcodeImageGenerator tokenGenerator;

	/**
	 * Constructor of the class TokenServiceImpl with a TokenRepository and an
	 * EventSender as parameters
	 * 
	 * @param tokenGenerator The token generator for generating tokens
	 * @param repository
	 *            TokenRepository where we save our tokens
	 * @param eventSender
	 *            EventSender used to send messages through a queue
	 */
	public TokenServiceImpl(TokenBarcodeImageGenerator tokenGenerator, TokenRepository repository, EventSender eventSender) {
		this.tokenRepository = repository;
		this.eventSender = eventSender;
		this.tokenGenerator = tokenGenerator;
	}

	@Override
	public List<Token> requestTokens(String cprString, int quantity) throws TokenException, CprException {

		CprNumber cpr = new CprNumber(cprString);

		if (!tokenRepository.containsCustomer(cpr))
			throw new CprException("CPR number not registered as a customer");
		if (quantity < 1)
			throw new TokenException("Cannot request less than one token");
		if (quantity > 5)
			throw new TokenException("Cannot request more than five tokens");
		if (this.getUnusedTokens(cpr.toString()).size() > 1)
			throw new TokenException("Too many unused tokens to request any more");

		List<Token> tokens = generateTokens(quantity);
		tokenRepository.addTokens(cpr, tokens);
		return tokens;
	}

	/**
	 * 
	 * Generates n {@link int} tokenid's and returns them. 
	 * 
	 * @param n
     *     allowed object is
     *     {@link int }
	 * 
	 * @return
     *     possible object is
     *     {@link List<Token> }
	 */
	private List<Token> generateTokens(int n) {
		List<Token> tokens = new ArrayList<Token>();
		for (int i = 0; i < n; i++) {
			tokens.add(new Token());
		}
		return tokens;
	}



	@Override
	public void addCustomer(String cprString) throws CprException {

		CprNumber cpr = new CprNumber(cprString);

		if (tokenRepository.containsCustomer(cpr))
			throw new CprException("A customer with this cpr is already registered");
		this.tokenRepository.addCustomer(cpr);
	}

	@Override
	public Token getToken(String tokenIdString) throws TokenException, CprException {
		TokenId tokenId = new TokenId(tokenIdString);
		return tokenRepository.getToken(tokenId);
	}

	@Override
	public CprNumber getCprNumberByTokenId(String tokenIdString) throws TokenException {

		TokenId tokenId = new TokenId(tokenIdString);
		
		if (tokenRepository.isTokenUsed(tokenId)) 
			throw new TokenException("Token is already used");
		
		CprNumber cpr = tokenRepository.getCprByTokenId(tokenId);

		if (cpr == null)
			throw new TokenException("No token with specified id");

		return cpr;
	}

	@Override
	public List<Token> getUnusedTokens(String cprString) throws CprException {

		CprNumber cpr = new CprNumber(cprString);
		
		if (!tokenRepository.containsCustomer(cpr))
			throw new CprException("CPR number not registered as a customer");
		
		List<Token> unusedTokens = new ArrayList<>();

		for (Token t : tokenRepository.getTokens(cpr))
			if (!t.isUsed())
				unusedTokens.add(t);

		return unusedTokens;
	}

	@Override
	public void setTokenAsUsed(String tokenId) throws TokenException {
		
		TokenId tokenIdObj = new TokenId(tokenId);
		this.tokenRepository.setTokenAsUsed(tokenIdObj);
	}
	
	@Override
	public boolean customerExists(String cprString) throws CprException {

		CprNumber cpr = new CprNumber(cprString);

		return tokenRepository.containsCustomer(cpr);
	}

	@Override
	public List<String> getAllTokens(String cprString) throws CprException {

		CprNumber cpr = new CprNumber(cprString);

		List<String> tokenIds = new ArrayList<>();

		for (Token t : tokenRepository.getTokens(cpr))
			tokenIds.add(t.getTokenId().toString());

		return tokenIds;
	}
 
	@Override
	public void receiveEvent(Event event) {
		if (event.getInstruction().equals("getTokensFromTokenRepositoryByCpr")) {			
			Event callbackEvent = new Event("getTokensFromTokenRepositoryByCprCallback");
			try {				
				List<Token> unusedTokens = getUnusedTokens((String) event.getArgs().get(0));
				Map<String, String> tokensUriById = makeResponceStructureForUnusedTokensRequest(unusedTokens);
				callbackEvent.addArg(tokensUriById);
			} catch (CprException e) {
				callbackEvent.setSuccessful(false);
				callbackEvent.setMessage(e.getMessage());
			}			
			sendEvent(callbackEvent);			
		} else if (event.getInstruction().equals("addCustomerToTokenRepository")) {
			String cpr = (String) event.getArgs().get(0);
			try {
				addCustomer(cpr);
			} catch (CprException e) {
				e.printStackTrace();
			}
		} else if (event.getInstruction().equals("cpr_by_token_id_request_event_customer_to_token")) {
			String tokenId = (String) event.getArgs().get(0);			
			Event callbackEvent = new Event("cpr_by_token_id_request_event_customer_to_tokenCallback");
			try {
				CprNumber cpr = getCprNumberByTokenId(tokenId);
				setTokenAsUsed(tokenId);
				callbackEvent.addArg(cpr.toString());
			} catch (TokenException e) {
				callbackEvent.setMessage(e.getMessage());
				callbackEvent.setSuccessful(false);
			}
			sendEvent(callbackEvent);
		}
	}

	private Map<String, String> makeResponceStructureForUnusedTokensRequest(List<Token> unusedTokens) {
		Map<String, String> tokensUriById = new LinkedHashMap<>();
		for(Token t:unusedTokens) {
			String tokenId = t.getTokenId().getId();
			tokensUriById.put(tokenId, makeTokenImagePath(tokenId));
		}
		return tokensUriById;
	}

	private String makeTokenImagePath(String tokenId) {
		return "/tokens/" + tokenId + "/image";
	}

	private void sendEvent(Event event) {
		try {
			eventSender.sendEvent(event);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}

	@Override
	public byte[] generateBarcodeImage(String tokenId) {
		return this.tokenGenerator.generateBarcodeImage(tokenId, 250,150,BarcodeFormat.CODE_128,"png");
	}
}
