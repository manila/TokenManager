package dk.manila.token_manager.interfaces;

import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

/**
 * Class for generating images from tokens
 *
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 */
public interface TokenBarcodeImageGenerator {
	
	/**
	 * Generates a byte array containing the image of a barcode representing the
	 * tokenId
	 * 
	 * @return A byte array containing the image of a barcode representing the
	 *         tokenId
	 * @throws WriterException
	 *             if it cannot write in the file
	 * @throws IOException
	 *             if it cannot write in the file
	 */
	byte[] generateBarcodeImage(String tokenId, int width, int heigh, BarcodeFormat barcodeFormat, String imageType) throws BarcodeImageGenerationException;
	
	public class BarcodeImageGenerationException extends RuntimeException{
		private static final long serialVersionUID = -4687165718899941581L;

		public BarcodeImageGenerationException(String msg){
			super(msg);
		}
	}

}
