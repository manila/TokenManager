package dk.manila.token_manager.interfaces;

import java.util.List;

import dk.manila.token_manager.queue.EventReceiver;
import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenRepresentation;

/**
 * Service containing all methods needed for the service
 * 
 * @author lukasvillumsen
 * 
 */
public interface TokenService {

	/**
	 * Request tokens
	 * 
	 * @param cpr
	 *            The customers CprNumber
	 * @param quantity
	 *            The amount of Tokens to generate (between 1-5)
	 * @return tokenRepresentations A list of tokens that has been generated
	 */
	public List<Token> requestTokens(String cpr, int quantity) throws TokenException, CprException;

	/**
	 * Add customer to the repository
	 * 
	 * @param cpr
	 *            The CprNumber of a new customer
	 * @throws CprException
	 */
	public void addCustomer(String cpr) throws CprException;

	/**
	 * Gets the TokenRepresentation corresponding to a tokenId
	 * 
	 * @param tokenId
	 *            The Tokens id that the customer wants to fetch
	 * @return A TokenRepresentation of the Token
	 * @throws TokenException
	 * @throws CprException
	 */
	public Token getToken(String tokenId) throws TokenException, CprException;

	/**
	 * Get the CprNumber of a customer that has the given token
	 * 
	 * @param tokenId
	 *            The ID of a Token belonging to the customer
	 * @return The CprNumber of the customer that has the given token
	 * @throws TokenException
	 */
	public CprNumber getCprNumberByTokenId(String tokenId) throws TokenException;

	/**
	 * Fetches all tokens marked as unused belonging to a CPR number
	 * 
	 * @param cpr
	 *            The CPR number to fetch tokens from
	 * @return unusedTokens List of token representations of unused tokens
	 * @throws CprException
	 */
	public List<Token> getUnusedTokens(String cpr) throws CprException;

	/**
	 * Checks if a customer with the specified CPR number exists in the repository
	 * 
	 * @param cpr
	 *            The CPR number to check for
	 * @return exists Boolean value reflecting the existence of the customer in the
	 *         repository
	 */
	public boolean customerExists(String cpr) throws CprException;

	/**
	 * Retrieves all tokens belonging to a customer with the specified CPR number
	 * 
	 * @param cpr
	 *            The CPR number to fetch for
	 * @return tokens List of token representation for all the tokens belonging to
	 *         the CPR
	 */
	public List<String> getAllTokens(String cpr) throws CprException;

	/**
	 * 
	 * Generates an image from tokenid {@link String}
	 * 
	 * @param tokenId
     *     allowed object is
     *     {@link String }
	 * @return
     *     possible object is
     *     {@link byte[] }
	 */
	public byte[] generateBarcodeImage(String tokenId);

	/**
	 * Set Token as used
	 * 
	 * @param tokenId
	 * @throws TokenException
	 */
	public void setTokenAsUsed(String tokenId) throws TokenException;
	
}
