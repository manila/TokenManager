package dk.manila.token_manager.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.manila.token_manager.exception.CprException;

/**
 * Class representing a CPR number
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class CprNumber extends IdValueObject {
	private final String CPR_REGEX = "\\d{10}";

	/**
	 * Constructor of the class CprNumber
	 * 
	 * @param cpr
	 *            CPR number we want to save
	 * @throws CprException
	 *             if the cpr is not correct
	 */
	public CprNumber(String cpr) throws CprException {
		super(cpr);
		if (cpr == null)
			throw new CprException("Invalid CPR number");
		Pattern pattern = Pattern.compile(CPR_REGEX);
		Matcher matcher = pattern.matcher(cpr);
		if (!matcher.matches())
			throw new CprException("Invalid CPR number");
	}

}