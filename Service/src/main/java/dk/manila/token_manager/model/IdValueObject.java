package dk.manila.token_manager.model;

/**
 * Class representing an immutable object
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public abstract class IdValueObject {
	private final String id;

	/**
	 * Constructor of the class IdValueObject
	 * 
	 * @param id
	 *            information we want to save
	 */
	public IdValueObject(String id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdValueObject other = (IdValueObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id;
	}

}
