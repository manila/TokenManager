package dk.manila.token_manager.model;

import java.util.UUID;

/**
 * Class representing a Token
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class Token {

	private TokenId tokenId;
	private boolean used;
	
	/**
	 * Default constructor of Token
	 */
	public Token() {
		this.tokenId = new TokenId(UUID.randomUUID().toString());
		this.used = false;
	}
	
	/**
	 * Gets the TokenId.
	 * @return the TokenId
	 */
	public TokenId getTokenId() {
		return tokenId;
	}
	
	/**
	 * Returns if the token is used.
	 * @return true if the token is used
	 */
	public boolean isUsed() {
		return used;
	}
	
	/**
	 * Sets the used status of the token.
	 * @param used true if it used
	 */
	public void setUsed(boolean used) {
		this.used = used;
	}
	
	protected void setTokenId(TokenId tokenId) {
		this.tokenId = tokenId;
	}
}
