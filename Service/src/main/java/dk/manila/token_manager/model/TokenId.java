package dk.manila.token_manager.model;

/**
 * Class representing a TokenId
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 */
public class TokenId extends IdValueObject {

	/**
	 * Constructor of the class TokenId
	 * 
	 * @param tokenId
	 *            the tokenId we want to save
	 */
	public TokenId(String tokenId) {
		super(tokenId);
	}
}