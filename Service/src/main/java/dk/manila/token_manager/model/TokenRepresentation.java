package dk.manila.token_manager.model;

/**
 * 
 * Representation of a Token This class is used when a token needs to be
 * presented for a customer
 * 
 * @author sebastiannyholm
 * 
 *
 */
public class TokenRepresentation {

	private String tokenId;
	private String barcodeUri;

	/**
	 * Constructor of the class TokenRepresentation
	 * 
	 * @param tokenId
	 *            used in the TokenRepresentation
	 */
	public TokenRepresentation(String tokenId) {
		this.tokenId = tokenId;
		this.barcodeUri = "/tokens/" + tokenId + "/image";
	}

	/**
	 * Gets the TokenId.
	 * 
	 * @return the TokenId
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * Gets the URI of the barcode.
	 * 
	 * @return URI of the barcode
	 */
	public String getBarcodeUri() {
		return barcodeUri;
	}

}
