package dk.manila.token_manager.queue;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing an event
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 */
public class Event {

	private String instruction;
	private List<Object> args = new ArrayList<Object>();
	private String message = "Success";
	private boolean successful = true;

	/**
	 * Constructor of an Event
	 * 
	 * @param instruction
	 *            instruction representing what the receiver should do with this
	 *            Event
	 */
	public Event(String instruction) {
		this.instruction = instruction;
	}

	/**
	 * Returns the instruction enclosed in the Event
	 * 
	 * @return the instruction enclosed in the Event
	 */
	public String getInstruction() {
		return this.instruction;
	}

	/**
	 * Returns the arguments enclosed in the Event
	 * 
	 * @return the arguments enclosed in the Event
	 */
	public List<Object> getArgs() {
		return this.args;
	}
	
	/**
	 * Returns the message enclosed in the Event
	 * 
	 * @return the message enclosed in the Event
	 */
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Returns the status enclosed in the Event
	 * 
	 * @return the status enclosed in the Event
	 */
	public boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	/**
	 * Add argument to the event
	 * 
	 * @param obj
	 */
	public void addArg(Object obj) {
		args.add(obj);
	}


}
