package dk.manila.token_manager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * A sender of events to a queue
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 * Inspired by the implementation given by Hubert Baumeister
 *
 */
public interface EventSender {

	/**
	 * Sends an event to a queue.
	 * 
	 * @param event
	 *            the event we want to send
	 * @throws IOException
	 *             if the connection to the queue cannot be established
	 * @throws TimeoutException
	 *             if it takes to much time to send the request
	 */
	public void sendEvent(Event event) throws IOException, TimeoutException;
}
