package dk.manila.token_manager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * A listener of a Rabbit Mq queue
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 * Inspired by the implementation given by Hubert Baumeister
 *
 */
public class RabbitMqListener {
	private EventReceiver eventReceiver;

	/**
	 * Constructor of the class RabbitMqListener
	 * 
	 * @param eventReceiver
	 *            the receiver of the events coming from the queue
	 */
	public RabbitMqListener(EventReceiver eventReceiver) {
		this.eventReceiver = eventReceiver;
	}
 
	/**
	 * Listens to a Rabbit Mq queue and sends the events to the EventReceiver
	 * 
	 * @param exchangeName
	 *            exchange name of the queue
	 * @param exchangeHost
	 *            name of the rabbit mq server
	 * @param exchangeType
	 *            type of exchange for the queue
	 * @throws IOException
	 *             if the connection to the queue cannot be established
	 * @throws TimeoutException
	 *             if it takes to much time to send the request
	 */
	public void listen(String exchangeName, String exchangeHost, String exchangType)
			throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(exchangeHost);
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchangeName, exchangType);
		channel.queueDeclare(exchangeName, false, false, false, null);

		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, exchangeName, "");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {

			String message = new String(delivery.getBody());
			Event event = new Gson().fromJson(message, Event.class);

			eventReceiver.receiveEvent(event);
		};

		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
		});
	}
}
