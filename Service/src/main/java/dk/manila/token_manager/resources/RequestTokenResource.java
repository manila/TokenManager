package dk.manila.token_manager.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A resource used to receive necessary information to create to tokens from a REST interface
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
@XmlRootElement
public class RequestTokenResource {
	@XmlElement public String cprNumber;
	@XmlElement public int quantity;	
}
