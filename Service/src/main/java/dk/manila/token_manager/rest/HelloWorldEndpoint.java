package dk.manila.token_manager.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Endpoint used to ping if the service is working
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
@Path("/hello")
public class HelloWorldEndpoint {

	/**
	 * Returns an HTTP response 200 if the service can be reached.
	 * 
	 * @return an HTTP response 200 if the service can be reached
	 */
	@GET
	@Produces("text/plain")
	public Response doGet() {
		return Response.ok("Hello from Token Manager!").build();
	}

}
