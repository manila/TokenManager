package dk.manila.token_manager.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.token_manager.impl.TokenBarcodeImageGeneratorImpl;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.queue.EventReceiver;
import dk.manila.token_manager.queue.RabbitMqListener;
import dk.manila.token_manager.queue.RabbitMqSender;

/**
 * Application that starts a REST interface, creates TokenServiceImpl and starts
 * a queue listener
 * 
 * @author delezealexandre
 *
 */
@ApplicationPath("/")
public class RestApplication extends Application {

	private TokenService service;

	private final String EXCHANGE_NAME = "dtu_pay_exchange";
	private final String EXCHANGE_HOST = "rabbitmq";
	private final String EXCHANGE_TYPE = "fanout";

	/**
	 * Default constructor of the RestApplication
	 */
	public RestApplication() {

		service = new TokenServiceImpl(new TokenBarcodeImageGeneratorImpl(), new TokenRepositoryImpl(),
				new RabbitMqSender(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE));

		try {
			new RabbitMqListener((EventReceiver) service).listen(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (TimeoutException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("service", service);
		return map;
	}

}
