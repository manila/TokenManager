package dk.manila.token_manager.rest;

import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Object which creates a response we can send through a REST interface
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class RestResponse {

	private int statusCode;
	private String message;
	private Object data;

	private JSONObject json;

	/**
	 * Constructor of RestResponse
	 * 
	 * @param statusCode
	 *            status code of the http response
	 * @param message
	 *            message we want to deliver to the client
	 * @param object
	 *            object we want to send to the client
	 */
	public RestResponse(int statusCode, String message, Object object) {
		this.statusCode = statusCode;
		this.message = message;
		this.data = object;

		this.json = new JSONObject();
	}

	/**
	 * Gets the Response enclosed in the RestResponse.
	 * 
	 * @return the Response enclosed in the RestResponse
	 */
	public Response getResponse() {

		try {
			this.json.put("statusCode", this.statusCode);
			this.json.put("message", this.message);
			this.json.put("tokens", this.data);

		} catch (JSONException e) {

			this.statusCode = 400;
			this.message = e.getMessage();

			try {
				this.json.put("statusCode", this.statusCode);
				this.json.put("message", this.message);

			} catch (JSONException e1) {

				return Response.status(this.statusCode).build();
			}
		}

		return Response.status(this.statusCode).entity(this.json.toString()).build();
	}
}
