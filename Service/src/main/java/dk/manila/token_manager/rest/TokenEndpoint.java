package dk.manila.token_manager.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator.BarcodeImageGenerationException;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenRepresentation;
import dk.manila.token_manager.resources.RequestTokenResource;

/**
 * Endpoint for tokens
 * 
 * @author sebastiannyholm
 *
 *
 */
@Path("/tokens")
public class TokenEndpoint {

	private TokenService service;
	private final String SERVICE_KEY = "service";

	/**
	 * Constructor of a TokenEndpoint
	 * 
	 * @param app
	 *            the application that creates the endpoint
	 */
	public TokenEndpoint(@Context Application app) {
		service = (TokenService) app.getProperties().get(SERVICE_KEY);
	}

	/**
	 * Returns the HTTP response with the image of the barcode of a token id.
	 * 
	 * @param tokenId
	 *            token id we want the barcode
	 * @return the HTTP response with the image of the barcode of the token id
	 */
	@GET
	@Path("/{id}/image")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces("image/png")
	public Response getBarcode(@PathParam("id") String tokenId) {

		byte[] bytes;
		try {
			bytes = service.generateBarcodeImage(tokenId);
		} catch (BarcodeImageGenerationException e) {
			return Response.status(500).build();
		}

		return Response.ok(bytes).build();
	}

	/**
	 * Requests new tokens for a customer to the service and sends the generated
	 * tokens to the customer.
	 * 
	 * @param requestTokenModel
	 *            request of the customer
	 * @return the response with the generated tokens
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response requestTokens(RequestTokenResource requestTokenModel) {

		List<TokenRepresentation> tokens;

		try {
			tokens = generateTokenRepresentations(service.requestTokens(requestTokenModel.cprNumber, requestTokenModel.quantity));
		} catch (TokenException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		} catch (CprException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		}

		return new RestResponse(200, requestTokenModel.quantity + " tokens has been added to your account", tokens)
				.getResponse();
	}
	
	/**
	 * Generates a list of token representations from given list of tokens 
	 * 
	 * @param tokens
     *     allowed object is
     *     {@link List<Token> }
	 * @return
     *     possible object is
     *     {@link List<TokenRepresentation> }
	 */
	private List<TokenRepresentation> generateTokenRepresentations(List<Token> tokens) {
		List<TokenRepresentation> tokenReps = new ArrayList<TokenRepresentation>();
		for (Token t : tokens) {
			tokenReps.add(new TokenRepresentation(t.getTokenId().toString()));
		}
		return tokenReps;
	}
}
