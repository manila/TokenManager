package cucumber_tests;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * 
 * @author Toonw
 *
 */

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty"},
		features = {"classpath:features/"},
		glue = {"cucumber_tests"}
		)
public class RunCucumberTest {
		
}