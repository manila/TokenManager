package cucumber_tests.steps;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.impl.TokenBarcodeImageGeneratorImpl;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.model.TokenRepresentation;
import dk.manila.token_manager.queue.EventSender;
import dk.manila.token_manager.queue.RabbitMqSender;

/**
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 *
 */
public class RequestTokensTest {
	private String cpr;
	private CprNumber cprNumber;
	private int tokensLeft, tokensGenerated;
	private String errorMessage;
	
	private TokenId tokenId;
	private List<TokenRepresentation> tokens;
	private TokenRepresentation token;
	
	private TokenService service;
	
	public RequestTokensTest() { 
		TokenRepository repository = new TokenRepositoryImpl();
		EventSender eventSender = new RabbitMqSender("dtu_pay_exchange", "rabbitmq", "fanout");
		TokenBarcodeImageGenerator tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		service = new TokenServiceImpl(tokenGenerator, repository, eventSender);
	}
	
	@Given("^A registered customer with CPR number \"([^\"]*)\"$")
	public void a_registered_customer_with_CPR_number(String cpr) throws Exception {
	    try {
	    	this.cpr = cpr;
	    	this.service.addCustomer(this.cpr);
	    } catch (CprException e) {
	    	this.errorMessage = e.getMessage();
	    }
	}
	
	@Given("^A unregistered customer with CPR number \"([^\"]*)\"$")
	public void a_unregistered_customer_with_CPR_number(String cpr) throws Exception {
	    this.cpr = cpr;
	}

	@Given("^The customer has (\\d+) unused token\\(s\\) left$")
	public void the_customer_has_unused_token_s_left(int tokensLeft) throws Exception {
		try {
			this.tokensLeft = service.requestTokens(cpr, tokensLeft).size();
		} catch (CprException e) {
	    	this.errorMessage = e.getMessage();
		} catch (TokenException e) {
	    	this.errorMessage = e.getMessage();
		}
	}

	@When("^The customer requests (\\d+) token\\(s\\)$")
	public void the_customer_requests_token_s(int tokensRequested) throws Exception {
		try {
			this.tokensGenerated = service.requestTokens(cpr, tokensRequested).size();
		} catch (CprException e) {
	    	this.errorMessage = e.getMessage();
		} catch (TokenException e) {
	    	this.errorMessage = e.getMessage();
		}
	}

	@Then("^The customer gets back (\\d+) token\\(s\\)$")
	public void the_customer_gets_back_token_s(int tokensReceived) throws Exception {
	    assertEquals(tokensReceived, tokensGenerated);
	}

	@Then("^The customer has (\\d+) unused token\\(s\\) in total$")
	public void the_customer_has_unused_token_s_in_total(int tokensLeftInTotal) throws Exception {
	    assertEquals(tokensLeftInTotal, service.getUnusedTokens(cpr).size());
	}
	
	@Then("^The customer gets back an error message \"([^\"]*)\"$")
	public void the_customer_gets_back_an_error_message(String errMsg) throws Exception {
		assertEquals(errMsg, errorMessage);
	}

	@Given("^the customer has at least one unused token")
	public void the_customer_has_at_least_one_unused_token() throws Exception {
		this.tokens = generateTokenRepresentations(this.service.requestTokens(this.cpr, 1));
		this.tokenId = new TokenId(this.tokens.get(0).getTokenId());
	}	
	private List<TokenRepresentation> generateTokenRepresentations(List<Token> tokens) {
		List<TokenRepresentation> tokenReps = new ArrayList<TokenRepresentation>();
		for (Token t : tokens)
			tokenReps.add(new TokenRepresentation(t.getTokenId().toString()));
		return tokenReps;
	}

	@When("^The customer retrieves the token$")
	public void the_customer_retrieves_the_token() throws Exception {		
		token = generateTokenRepresentations(
				new ArrayList<Token>( Arrays.asList(service.getToken(tokenId.toString())) )
				).get(0);		
	}
	
	@Then("^The customer gets back the token")
	public void the_customer_gets_back_the_token() throws Exception {
		assertEquals(this.tokens.get(0).getTokenId(), this.token.getTokenId());
	}
	
	@When("^The cpr number is requested from a token id belonging to that customer$")
	public void the_cpr_number_is_requested_from_a_token_id_belonging_to_that_customer() throws Exception {
	    this.cprNumber = this.service.getCprNumberByTokenId(this.tokenId.toString());
	}

	@Then("^The cpr number \"([^\"]*)\" is returned$")
	public void the_cpr_number_is_returned(String cpr) throws Exception {
		assertEquals(cpr, this.cprNumber.toString());
	}
	
}
