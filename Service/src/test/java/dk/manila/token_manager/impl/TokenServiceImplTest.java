package dk.manila.token_manager.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.google.zxing.BarcodeFormat;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;

public class TokenServiceImplTest { 
	TokenServiceImpl tokenServiceImpl;
	@Rule
	public ExpectedException ee = ExpectedException.none();	 
		
	@Test
	public void givenRequestingTokenByTokenId_returnTokenRepoWasCalledOnceWithThatTokenId() throws CprException, TokenException {
		TokenRepoForGetokenById fakeTokenRepo = new TokenRepoForGetokenById();
		tokenServiceImpl = new TokenServiceImpl(null, fakeTokenRepo, null);
		tokenServiceImpl.getToken("1234567890");	
		assertEquals(1, fakeTokenRepo.numberOfCallsTo_getToken);
		assertEquals("1234567890", fakeTokenRepo.tokenIdThat_getToken_WasCalledWith);		
	}
	
	@Test
	public void givenAddingANonExistingCutomerByCpr_returnAddCustomerWasCalledOnceWithSameCpr() throws CprException {
		TokenRepoForAddNonExistingCustomer fakeTokenRepo = new TokenRepoForAddNonExistingCustomer();
		tokenServiceImpl = new TokenServiceImpl(null, fakeTokenRepo, null);
		tokenServiceImpl.addCustomer("1234567890");	
		assertEquals(1, fakeTokenRepo.numberOfCallsTo_addCustomer);
		assertEquals("1234567890", fakeTokenRepo.cprThat_addCustomer_WasCalledWith);		
	}
	
	@Test
	public void givenAddingExistingCutomerByCpr_returnCprException() throws CprException {
		tokenServiceImpl = new TokenServiceImpl(null, new TokenRepository() {

			@Override
			public void addToken(CprNumber cpr, Token t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				return true;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				return null;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}			
		}, null);
		
		ee.expect(CprException.class);
		ee.expectMessage("A customer with this cpr is already registered");
		tokenServiceImpl.addCustomer("1234567890");
	} 
	
	@Test
	public void givenUserHasTwoTokensAndRequestsAllTokens_returnTheTwoTokens() throws CprException {
		tokenServiceImpl = new TokenServiceImpl(null, new TokenRepository() {

			@Override
			public void addToken(CprNumber cpr, Token t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				return new ArrayList<Token>(Arrays.asList(new Token(), new Token()));
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}			
		}, null);
		List<String> tokenIds = tokenServiceImpl.getAllTokens("1234567890");
		assertEquals(2, tokenIds.size());
	}
	
	@Test
	public void givenCprOfRegisteredCustomer_returnCustomerExist() throws CprException {
		tokenServiceImpl = new TokenServiceImpl(null, new TokenRepository(){

			@Override
			public void addToken(CprNumber cpr, Token t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				return true;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}
			
		}, null);
		assertEquals(true, tokenServiceImpl.customerExists("1234567890"));		
	} 
	@Test
	public void givenCprOfRegisteredCustomer_returnCustomerDoesNotExist() throws CprException {
		tokenServiceImpl = new TokenServiceImpl(null, new TokenRepository(){

			@Override
			public void addToken(CprNumber cpr, Token t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				return false;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}
			
		}, null);
		assertEquals(false, tokenServiceImpl.customerExists("1234567890"));		
	}
	
	
	@Test
	public void givenUnregisteredCustomerTryingToRequestTokens_returnNumberOfTokensAddedToReoisitory() throws TokenException, CprException {
		ee.expect(CprException.class);
		ee.expectMessage("CPR number not registered as a customer");
		 
		tokenServiceImpl = new TokenServiceImpl(null, getRepositoryOnContainsCustomerRetursnFalse(), null);
		tokenServiceImpl.requestTokens("1234567890", 2);
	}	
	
	@Test
	public void givenRegisteredCustomerWithMoreThanOneTokensLeftIsTryingToRequestNewToken_returnTokenException() throws TokenException, CprException {
		ee.expect(TokenException.class);
		ee.expectMessage("Too many unused tokens to request any more");
		 
		tokenServiceImpl = new TokenServiceImpl(null, getRepositoryOnGetTokensReturnsMoreThanOneTokens(), null);
		tokenServiceImpl.requestTokens("1234567890", 2);
	}		
	@Test
	public void givenRegisteredCustomerTryingToRequestMoreThanFiveTokens_returnTokenException() throws TokenException, CprException {
		ee.expect(TokenException.class);
		ee.expectMessage("Cannot request more than five tokens");
		 
		tokenServiceImpl = new TokenServiceImpl(null, getRepositoryOnContainsCustomerRetursnTrue(), null);
		tokenServiceImpl.requestTokens("1234567890", 6);
	}	
	@Test
	public void givenRegisteredCustomerTryingToRequestLessThanOneTokens_returnTokenException() throws TokenException, CprException {
		ee.expect(TokenException.class);
		ee.expectMessage("Cannot request less than one token");
		 
		tokenServiceImpl = new TokenServiceImpl(null, getRepositoryOnContainsCustomerRetursnTrue(), null);
		tokenServiceImpl.requestTokens("1234567890", 0);
	}	 
	@Test
	public void givenRegisteredCustomerRequestsNumberOfTokensByCpr_returnTheSameNumberOfTokensAndCpr() throws TokenException, CprException {
		FakeTokenRepoForCprAndTokensAdded fakeTokeRepo = new FakeTokenRepoForCprAndTokensAdded();
		tokenServiceImpl = new TokenServiceImpl(null, fakeTokeRepo, null);
		tokenServiceImpl.requestTokens("1234567890", 2);
		assertEquals(2 + 1, fakeTokeRepo.numberOfTokens);
		assertEquals(1, fakeTokeRepo.numberOfCallsToAddTokens);
		assertEquals("1234567890", fakeTokeRepo.cprNumber);
	}		
	private TokenRepository getRepositoryOnContainsCustomerRetursnTrue() {
		return new TokenRepository() {

			@Override
			public void addToken(CprNumber cpr, Token t) {	}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				
				return true;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}
			
		};
	}
	private TokenRepository getRepositoryOnGetTokensReturnsMoreThanOneTokens() {
		return new TokenRepository() {

			@Override
			public void addToken(CprNumber cpr, Token t) {	}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				
				return true;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				return new ArrayList<Token>(Arrays.asList(new Token(),new Token()));
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}
			
		};
	}
	
	
	class TokenRepoForGetokenById implements TokenRepository{
		public String tokenIdThat_getToken_WasCalledWith;
		public int numberOfCallsTo_getToken;
		@Override
		public Token getToken(TokenId tokenId) {
			tokenIdThat_getToken_WasCalledWith = tokenId.getId();
			numberOfCallsTo_getToken++;
			return new Token();
		}
		@Override
		public void addToken(CprNumber cpr, Token t) {
			
		}

		@Override
		public void addTokens(CprNumber cpr, List<Token> tokens) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean containsCustomer(CprNumber cpr) {
			return false;
		}
		
		@Override
		public void addCustomer(CprNumber cpr) {
			
		}
		@Override
		public CprNumber getCprByTokenId(TokenId tokenId) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<Token> getTokens(CprNumber cpr) {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public void setTokenAsUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean isTokenUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			return false;
		}
	}
	
	class TokenRepoForAddNonExistingCustomer implements TokenRepository{

		@Override
		public void addToken(CprNumber cpr, Token t) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addTokens(CprNumber cpr, List<Token> tokens) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean containsCustomer(CprNumber cpr) {
			return false;
		}
		public String cprThat_addCustomer_WasCalledWith;
		public int numberOfCallsTo_addCustomer;
		
		@Override
		public void addCustomer(CprNumber cpr) {
			cprThat_addCustomer_WasCalledWith = cpr.getId();
			numberOfCallsTo_addCustomer++;
		}

		@Override
		public Token getToken(TokenId tokenId) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CprNumber getCprByTokenId(TokenId tokenId) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<Token> getTokens(CprNumber cpr) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setTokenAsUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isTokenUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	class FakeTokenRepoForCprAndTokensAdded implements TokenRepository{
			public String cprNumber;
			public int numberOfTokens;
			public int numberOfCallsToAddTokens;
			private List<Token> totalUnusedTokens = new ArrayList<Token>(Arrays.asList(new Token()));
			
			@Override
			public void addToken(CprNumber cpr, Token t) {	}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				totalUnusedTokens.addAll(tokens);
				cprNumber = cpr.getId();
				numberOfTokens = totalUnusedTokens.size();
				numberOfCallsToAddTokens++;
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				
				return true;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				return totalUnusedTokens;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}

			
		}
	private TokenRepository getRepositoryOnContainsCustomerRetursnFalse() {
		return new TokenRepository() {

			@Override
			public void addToken(CprNumber cpr, Token t) {	}

			@Override
			public void addTokens(CprNumber cpr, List<Token> tokens) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean containsCustomer(CprNumber cpr) {
				
				return false;
			}

			@Override
			public void addCustomer(CprNumber cpr) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Token getToken(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CprNumber getCprByTokenId(TokenId tokenId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Token> getTokens(CprNumber cpr) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setTokenAsUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isTokenUsed(TokenId tokenId) {
				// TODO Auto-generated method stub
				return false;
			}
			
		};
	}
	
	@Test
	public void givenRequestNumberOfTokensForCpr_returnNumberOfTokensAddedToReoisitory() {
		tokenServiceImpl = new TokenServiceImpl(null, null, null);
	}	
	class FakeTokenRepository implements TokenRepository{

		@Override
		public void addToken(CprNumber cpr, Token t) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addTokens(CprNumber cpr, List<Token> tokens) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean containsCustomer(CprNumber cpr) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void addCustomer(CprNumber cpr) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Token getToken(TokenId tokenId) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CprNumber getCprByTokenId(TokenId tokenId) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<Token> getTokens(CprNumber cpr) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setTokenAsUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isTokenUsed(TokenId tokenId) {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	@Test
	public void givenRequestBarcodeImageByTokenId_returnBarcodeGeneratorWasCalledWithTheTokenId() {
		//arrange
		FakeTokenBarcodeImageGenerator fakeGenerator = new FakeTokenBarcodeImageGenerator();
		tokenServiceImpl = new TokenServiceImpl(fakeGenerator, null, null);
		
		//act
		tokenServiceImpl.generateBarcodeImage("1234567890");
		
		//assert
		assertEquals("1234567890", fakeGenerator.generateBarcodeImageCalledWith_tokenId);
	}
	
	class FakeTokenBarcodeImageGenerator implements TokenBarcodeImageGenerator{
		public String generateBarcodeImageCalledWith_tokenId = "";
		@Override
		public byte[] generateBarcodeImage(String tokenId, int width, int heigh, BarcodeFormat barcodeFormat,
				String imageType) throws BarcodeImageGenerationException {
			generateBarcodeImageCalledWith_tokenId = tokenId;
			return null;
		}		
	}
}
