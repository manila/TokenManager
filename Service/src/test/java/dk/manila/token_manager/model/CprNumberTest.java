package dk.manila.token_manager.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import dk.manila.token_manager.exception.CprException;

/**
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 * @author Toonw
 *
 */
public class CprNumberTest {

	
	@Test
	public void givenCompareTwoCprNumberObjectsWithTheSameId_returnEqual() throws CprException {
		CprNumber firstCprNumberObject = new CprNumber("1234567890");
		CprNumber secondCprNumberObject = new CprNumber("1234567890");
		assertEquals(true, firstCprNumberObject.equals(secondCprNumberObject));
		assertEquals(true, firstCprNumberObject.equals(firstCprNumberObject));
		assertEquals(firstCprNumberObject.hashCode(),secondCprNumberObject.hashCode());
		assertEquals(firstCprNumberObject.toString(),secondCprNumberObject.toString());
	} 
	
	@Test
	public void givenCompareTwoCprNumberObjectWithDifferentId_returnNotEqual() throws CprException {
		CprNumber firstCprNumberObject = new CprNumber("1234567890");
		CprNumber secondCprNumberObject = new CprNumber("2234567890");
		assertEquals(false, firstCprNumberObject.equals(secondCprNumberObject));
		assertEquals(false, firstCprNumberObject.equals(null));
		assertEquals(false, firstCprNumberObject.equals("1234567890"));	
		assertNotEquals(firstCprNumberObject.hashCode(),secondCprNumberObject.hashCode());
		assertNotEquals(firstCprNumberObject.toString(),secondCprNumberObject.toString());
	} 
	
	@Test
	public void givenValidCprString_returnTheCprWasSet() throws CprException {
		String validCpr = "1234578900";
		CprNumber cprNumber = new CprNumber(validCpr);
		assertEquals("1234578900", cprNumber.getId());
	}
	 
	@Test(expected = CprException.class)
	public void givenCprStringWithLessThan10Figures_returnCastCprException() throws CprException {
		String invalidCpr = "34578900";
		//act
		new CprNumber(invalidCpr);		
	}
	
	@Test(expected = CprException.class)
	public void givenAlfanumericCprString_returnCastCprException() throws CprException {
		String invalidCpr = "34578900af33";
		//act
		new CprNumber(invalidCpr);		
	}
		
	@Test(expected = CprException.class)
	public void givenNullCprString_returnCastCprException() throws CprException {
		new CprNumber(null);
	}
	
	@Rule
	public ExpectedException expected = ExpectedException.none();		
	@Test
	public void givenInvalidCprString_returnCastCprExceptionWithMessage() throws CprException {
		expected.expect(CprException.class);
	    expected.expectMessage("Invalid CPR number");
		String invalidCpr = "34578900";
		//act
		new CprNumber(invalidCpr);				
	}
}
