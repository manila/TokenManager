package dk.manila.token_manager.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 * @author Toonw
 *
 */
public class TokenTest {
	@Test
	public void givenUnusedToken_returnTokenIsUnused() {
		assertEquals(false, new Token().isUsed());
	}
	
	@Test 
	public void givenUsedToken_returnTokenIsUsed() {
		//arrange
		Token token = new Token();
		//act
		token.setUsed(true);
		//assert
		assertEquals(true, token.isUsed());
	}
	
	@Test 
	public void givenTokenId_returnTheSameTokenId() {
		Token token = new Token();
		TokenId tokenId = new TokenId("1234");
		token.setTokenId(tokenId);
		 
		assertEquals(tokenId, token.getTokenId());
	}
}
