package junit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

import dk.manila.token_manager.impl.TokenBarcodeImageGeneratorImpl;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenBarcodeImageGenerator;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenRepresentation;
import dk.manila.token_manager.queue.Event;
import dk.manila.token_manager.queue.EventSender;

/**
 * 
 * @author Alexandre, Kaloyan, Sebastian, Lukas
 * @author Toonw
 *
 */
public class IncomingQueueTest {
	
	private TokenServiceImpl service; 
	private EventSender sender;
	private TokenRepository repository;
	private TokenBarcodeImageGenerator tokenGenerator;
	
	@Test
	public void addUserToRepository_WhenRequestFromQueue() throws Exception {
		//arrange
		
		CprNumber cpr = new CprNumber("8529348601");
		
		repository = new TokenRepositoryImpl();;
		tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		sender = new EventSender() {			
			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				
			}
		};
		
		service = new TokenServiceImpl(tokenGenerator, repository, sender);
		
		//act
		Event event = new Event("addCustomerToTokenRepository");
		event.addArg(cpr.toString());
		
		this.service.receiveEvent(event);
		
		//assert
		assertTrue(service.customerExists(cpr.toString()));
	}
	
	@Test
	public void getUserTokens_WhenRequestFromQueue() throws Exception {
		//arrange
		CprNumber cpr = new CprNumber("8529348600");
		Token t1 = new Token();
		Token t2 = new Token();
		
		repository = new TokenRepositoryImpl();
		repository.addCustomer(cpr);
		repository.addToken(cpr, t1);
		repository.addToken(cpr, t2);
		
		tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		
		sender = new EventSender() {

			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				
				//List<TokenRepresentation> tokens = (List<TokenRepresentation>) event.getArgs().get(0);
				List<TokenRepresentation> tokens = new ArrayList<>();

				Map<String, String> tokensUriById = (Map<String, String>) event.getArgs().get(0);
				List<TokenRepresentation> tokensReprs = new ArrayList<>();
				for(String tokenId : tokensUriById.keySet()) {
					tokensReprs.add(new TokenRepresentation(tokenId));
				}
				
				assertEquals(2, tokensReprs.size());
				
			}
		};
		
		service = new TokenServiceImpl(tokenGenerator, repository, sender);
		
		List<TokenRepresentation> tokens = generateTokenRepresentations(service.getUnusedTokens(cpr.toString()));
		assertEquals(2, tokens.size());
		
		//act
		Event event = new Event("getTokensFromTokenRepositoryByCpr");
		event.addArg(cpr.toString());
		
		this.service.receiveEvent(event);
		
		//assert
		// check the mocked event sender
	}
	
	@Test
	public void getUserNotExist_WhenRequestFromQueue() throws Exception {
		//arrange
		
		// Cpr number not added to repository
		CprNumber cpr = new CprNumber("8529348601");
		repository = new TokenRepositoryImpl();;
		
		tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		
		sender = new EventSender() {

			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				
				assertEquals("CPR number not registered as a customer", event.getMessage());
				
			}
		};
		
		service = new TokenServiceImpl(tokenGenerator, repository, sender);
		
		//act
		Event event = new Event("getTokensFromTokenRepositoryByCpr");
		event.addArg(cpr.toString());
		
		this.service.receiveEvent(event);
		
		//assert
		// check the mocked event sender
	}
	private List<TokenRepresentation> generateTokenRepresentations(List<Token> tokens) {
		List<TokenRepresentation> tokenReps = new ArrayList<TokenRepresentation>();
		for (Token t : tokens) {
			tokenReps.add(new TokenRepresentation(t.getTokenId().toString()));
		}
		return tokenReps;
	}
	@Test
	public void givenOneUnusedToken_SetItToUsedWhenGettingThePayRequestFromQueue() throws Exception {
		//arrange
		CprNumber cpr = new CprNumber("8529348602");
		Token t = new Token();
		
		repository = new TokenRepositoryImpl();
		repository.addCustomer(cpr);
		repository.addToken(cpr, t);
		
		tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		
		sender = new EventSender() {

			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				
				
			}
		};
		
		service = new TokenServiceImpl(tokenGenerator, repository, sender);
		
		List<TokenRepresentation> tokens = generateTokenRepresentations(service.getUnusedTokens(cpr.toString()));
		assertEquals(1, tokens.size());
		
		//act
		Event event = new Event("cpr_by_token_id_request_event_customer_to_token");
		event.addArg(t.getTokenId().toString());
		
		this.service.receiveEvent(event);
		
		//assert
		
		tokens = generateTokenRepresentations(service.getUnusedTokens(cpr.toString()));
		assertEquals(0,  tokens.size()); 
	}
	
	@Test
	public void givenOneUsedToken_ReturnEmptyListWhenGettingThePayRequestFromQueue() throws Exception {
		//arrange
		CprNumber cpr = new CprNumber("8529348603");
		Token t = new Token();
		
		repository = new TokenRepositoryImpl();
		repository.addCustomer(cpr);
		repository.addToken(cpr, t);
		
		tokenGenerator = new TokenBarcodeImageGeneratorImpl();
		
		sender = new EventSender() {

			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				
				assertEquals("Token is already used",  event.getMessage());
				
			}
		};
		
		service = new TokenServiceImpl(tokenGenerator, repository, sender);
		
		List<TokenRepresentation> tokens = generateTokenRepresentations(service.getUnusedTokens(cpr.toString()));
		assertEquals(1, tokens.size());
		repository.setTokenAsUsed(t.getTokenId());
		
		//act
		Event event = new Event("cpr_by_token_id_request_event_customer_to_token");
		event.addArg(t.getTokenId().toString());
		
		this.service.receiveEvent(event);
		
		//assert
		// check the mocked event sender
	}
}
