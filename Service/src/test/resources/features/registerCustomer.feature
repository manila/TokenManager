## @author sebastiannyholm, lukasviilumsen
Feature: Registering customers
      
	Scenario: Unregistered customer registering with valid CPR number
		Given An unregistered customer with a valid CPR number
		When The customer registers with the token service
		Then The customer exists in the repository
		And The customer has 0 tokens
		
	Scenario: Unregistered customer registering with invalid CPR number
		Given An unregistered customer with an invalid CPR number
		When The customer registers with the token service
		Then An error message is returned saying "Invalid CPR number"
	
	Scenario: Registered customer registering again
		Given A registered customer with a valid CPR number
		When The customer registers with the token service
		Then An error message is returned saying "A customer with this cpr is already registered"
		