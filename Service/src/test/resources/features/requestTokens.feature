## @author lukasvillumsen
Feature: Requesting tokens
      
	Scenario: Registered customer requesting the minimum valid amount of tokens
		Given A registered customer with CPR number "1708921402" 
		And The customer has 0 unused token(s) left
		When The customer requests 1 token(s)
		Then The customer gets back 1 token(s)
		
	Scenario: Registered customer requesting the maximum valid amount of tokens
		Given A registered customer with CPR number "1708921402" 
		And The customer has 0 unused token(s) left
		When The customer requests 5 token(s)
		Then The customer gets back 5 token(s)

	Scenario: Registered customer requesting a valid amount of tokens
		Given A registered customer with CPR number "1708921402" 
		And The customer has 0 unused token(s) left
		When The customer requests 3 token(s)
		Then The customer gets back 3 token(s)

	Scenario: Registered customer can request a valid amount of token with one unused token left
		Given A registered customer with CPR number "1708921402" 
		And The customer has 1 unused token(s) left
		When The customer requests 5 token(s)
		Then The customer gets back 5 token(s)
		And The customer has 6 unused token(s) in total

	Scenario: Registered customer cannot request any amount of tokens with more than one unused token left
		Given A registered customer with CPR number "1708921402" 
		And The customer has 2 unused token(s) left
		When The customer requests 2 token(s)
		Then The customer gets back an error message "Too many unused tokens to request any more" 
		And The customer has 2 unused token(s) in total
	
	Scenario: Registered customer cannot request less than one token
		Given A registered customer with CPR number "1708921402" 
		And The customer has 0 unused token(s) left
		When The customer requests 0 token(s)
		Then The customer gets back an error message "Cannot request less than one token" 
		And The customer has 0 unused token(s) in total
	
	Scenario: Registered customer cannot request more than five tokens
		Given A registered customer with CPR number "1708921402" 
		And The customer has 0 unused token(s) left
		When The customer requests 6 token(s)
		Then The customer gets back an error message "Cannot request more than five tokens" 
		And The customer has 0 unused token(s) in total
	
	Scenario: Registered customer cannot request tokens with invalid CPR number
		Given A registered customer with CPR number "invalid-cpr-format"
		Then The customer gets back an error message "Invalid CPR number"
	
	Scenario: Unregistered customer cannot request tokens
		Given A unregistered customer with CPR number "1708921403" 
		When The customer requests 1 token(s)
  		Then The customer gets back an error message "CPR number not registered as a customer"
	
	Scenario: Registered customer retrieves one token from his stock
		Given A registered customer with CPR number "1708921402" 
		And the customer has at least one unused token 
		When The customer retrieves the token
		Then The customer gets back the token
		
	Scenario: Getting back customer cpr number when supplied with a token id
		Given A registered customer with CPR number "1708921402" 
		And the customer has at least one unused token 
		When The cpr number is requested from a token id belonging to that customer
		Then The cpr number "1708921402" is returned
	