package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */

public class HelloWorldEndpointTest {

	private WebTarget webTarget;
	
	@Before
	public void setUp() {
		
		Client client = ClientBuilder.newClient();
		webTarget = client.target("http://02267-manila.compute.dtu.dk:8004");
		
	}
	
	@Test
	public void TestConnection() {
		
		webTarget = webTarget.path("hello");
		Response response = webTarget.request().get();
		
		assertEquals(200, response.getStatus());
		
	}
}