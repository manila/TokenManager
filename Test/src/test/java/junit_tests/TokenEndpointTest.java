package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import resources.RequestTokenResource;

/**
 * 
 * @author lukasvillumsen, sebastiannyholm
 *
 */
public class TokenEndpointTest {

	private WebTarget webTarget;
	
	@Before
	public void setUp() {
		Client client = ClientBuilder.newClient();
		webTarget = client.target("http://02267-manila.compute.dtu.dk:8004");
	}
	
	@Test
	public void postRequestTokenUnregisteredCustomerTest() {
		
		RequestTokenResource tokenModel = new RequestTokenResource();
		tokenModel.quantity = 3;
		tokenModel.cprNumber = "0123456789";
		
		webTarget = webTarget.path("tokens");
		Response response = webTarget.request() 
				.post(Entity.entity(tokenModel, MediaType.APPLICATION_JSON));
		
		String message = "";
		try {
			JSONObject json = new JSONObject(response.readEntity(String.class));
			message = (String) json.get("message");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		assertEquals(400, response.getStatus());
		assertEquals("CPR number not registered as a customer", message);
	}

}